import vector;
import misc;
import gfx;
import ui;
import std.math;
import std.conv;
import std.exception;
import std.string;
import std.stdio;
import glad.gl.all;
import glad.gl.loader;
import gl3n.math;
import gl3n.linalg;
import sdl2;
static import ogl_renderer;

public uint Renderer_WindowFlags=SDL_WINDOW_OPENGL;
public alias RendererTexture_t=void*;
public immutable float Renderer_SmokeRenderSpeed=.4+.35*Program_Is_Optimized;
alias RendererParticleSize_t=uint;

private ogl_renderer.Renderer Renderer;

extern(C){
	char *gluErrorString(GLenum);
	void gluLookAt(GLdouble, GLdouble, GLdouble, GLdouble, GLdouble, GLdouble, GLdouble, GLdouble, GLdouble);
	void glMatrixMode(GLenum);
	void glLoadIdentity();
	enum GL_MATRIX_MODE                          =0x0BA0;
	enum GL_MODELVIEW                            =0x1700;
	enum GL_PROJECTION                           =0x1701;
	enum GL_TEXTURE                              =0x1702;
}

struct Map_t{
	uint xsize, ysize, zsize;
	union __voxel_t{
		uint icol;
		struct{
			ubyte S, B, G, R;
		}
		this(uint data, bool is_solid){
			R=(data&255); G=(data>>8)&255; B=(data>>16)&255; S=is_solid ? 255 : 0;
		}
	}
	__voxel_t[] mapdata;
	ref __voxel_t opIndex(Tx, Ty, Tz)(Tx xpos, Ty ypos, Tz zpos){
		return mapdata[(cast(size_t)(xpos))+(cast(size_t)(ypos))*xsize+(cast(size_t)(zpos))*xsize*ysize];
	}
	void opIndexAssign(Tx, Ty, Tz)(__voxel_t v, Tx xpos, Ty ypos, Tz zpos){
		mmapdata[(cast(size_t)(xpos))+(cast(size_t)(ypos))*xsize+(cast(size_t)(zpos))*xsize*ysize].icol=v.icol;
	}
	void opIndexAssign(Tx, Ty, Tz)(uint v, Tx xpos, Ty ypos, Tz zpos){
		mapdata[(cast(size_t)(xpos))+(cast(size_t)(ypos))*xsize+(cast(size_t)(zpos))*xsize*ysize].icol=v;
	}
	bool isSurfaceBlock(Tx, Ty, Tz)(Tx x, Ty y, Tz z){
		uint airsides=0;
		airsides+=x>0 ? this[x-1, y, z].S==0 : 1;
		airsides+=y>0 ? this[x, y-1, z].S==0 : 1;
		airsides+=z>0 ? this[x, y, z-1].S==0 : 1;
		airsides+=x<xsize-1 ? this[x+1, y, z].S==0 : 1;
		airsides+=y<ysize-1 ? this[x, y+1, z].S==0 : 1;
		airsides+=z<zsize-1 ? this[x, y, z+1].S==0 : 1;
		return airsides>0;
	}
}

Map_t Game_Map;

version(DigitalMars){
	@nogc pragma(inline, true):
}
nothrow T __Project2D(T)(Vector_t!(3, T) pos, ref signed_register_t scrx, ref signed_register_t scry){
	return 0.0;
}

RendererTexture_t Renderer_NewTexture(uint xsize, uint ysize, bool streaming_texture=false){
	return null;
}

RendererTexture_t Renderer_TextureFromSurface(SDL_Surface *srfc){
	return null;
}

void Renderer_UploadToTexture(SDL_Surface *srfc, RendererTexture_t tex){
}

uint[2] Renderer_TextureSize(RendererTexture_t tex){
	return [0, 0];
}

void Renderer_DestroyTexture(RendererTexture_t tex){
}

//Renderer rend=new Renderer();

SDL_GLContext context;

string _Load_GLFile(string fname){
	import std.file;
	return readText("Ressources/System/"~fname);
}

void Renderer_Init(){
	SDL_SetHint(SDL_HINT_RENDER_DRIVER, "opengl");
	SDL_SetHint(SDL_HINT_RENDER_OPENGL_SHADERS, "1");
	SDL_GL_SetAttribute(SDL_GLattr.SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GLattr.SDL_GL_CONTEXT_MINOR_VERSION, 6);
	int ret=SDL_GL_LoadLibrary(null);
	if(ret){
		writeflnerr("Couldn't load GL library: %s", fromStringz(SDL_GetError()));
		return;
	}

}

void Renderer_SetUp(uint screen_xsize, uint screen_ysize){
	if(!context)
		context=SDL_GL_CreateContext(scrn_window);
	if(!Renderer){
		Renderer=new ogl_renderer.Renderer(&_Load_GLFile);
	}
	Renderer.ResizeWindow(screen_xsize, screen_ysize);
}

void Renderer_SetCamera(real xrotation, real yrotation, real tilt, real xfov, real yfov, real xpos, real ypos, real zpos){
	Renderer.SetCamera(yfov, 1.0, xpos, ypos, zpos, xrotation, yrotation);
}

void Renderer_DrawSprite(in Sprite_t spr){
}

void Renderer_FillRect2D(SDL_Rect *rct, ubyte[4] *color){
}

void Renderer_SetLOD(float lod){
}

RendererParticleSize_t[3] Renderer_GetParticleSize(fVector3_t size){
	return [to!uint(sqrt(size.x*size.x*.5+size.z*size.z*.5)*ScreenXSize*.25), to!uint(size.y*ScreenYSize/3.0), 0];
}

void Renderer_Draw3DParticle(alias hole_side=false)(immutable in float x, immutable in float y, immutable in float z,
immutable in RendererParticleSize_t w, immutable in RendererParticleSize_t h, immutable in RendererParticleSize_t l, uint col){
}

void Renderer_LoadMap(uint maptype, uint mapxsize, uint mapysize, uint mapzsize, ubyte[] voxmap){
	Game_Map.xsize=mapxsize; Game_Map.ysize=mapysize; Game_Map.zsize=mapzsize;
	Game_Map.mapdata.length=mapxsize*mapysize*mapzsize;
	Game_Map.mapdata[]=Game_Map.__voxel_t(0, true);
	void SetVoxel(uint xpos, uint ypos, uint zpos, Game_Map.__voxel_t col){
		Game_Map.mapdata[xpos+ypos*mapxsize+zpos*mapxsize*mapysize]=col;
	}
	uint *columnptr=cast(uint*)voxmap.ptr;
	foreach(z; 0..mapzsize){
		foreach(x; 0..mapxsize){
			int y=0;
			while(1){
				int datasize=(*columnptr)&255, col_start2=((*columnptr)>>8)&255, col_end2=((*columnptr)>>16)&255;
				int col_height2=col_end2-col_start2;
				//Clearing air
				for(;y<col_start2; y++){
					SetVoxel(x, y, z, Game_Map.__voxel_t(0, false));
				}
				//First column
				uint *colorptr=columnptr-col_start2+1;
				for(y=col_start2; y<=col_end2; y++){
					SetVoxel(x, y, z, Game_Map.__voxel_t(colorptr[y], true));
				}
				if(!datasize){
					//Arrived at end of column
					columnptr+=(col_height2+2);
					break;
				}
				//Second column
				columnptr+=datasize;
				int airstart=(*columnptr>>24)&255;
				colorptr=columnptr-airstart;
				for(/*col_height2-datasize+airstart+2*/; y<airstart; y++){
					SetVoxel(x, y, z, Game_Map.__voxel_t(colorptr[y], true));
				}
			}
		}
	}
	/*SDL_Surface *srfc=SDL_CreateRGBSurface(0, 512, 512, 32, 0, 0, 0, 0);
	foreach(x; 0..512){
		foreach(z; 0..512){
			auto y=Voxel_GetHighestY(x, 0, z);
			(cast(uint*)srfc.pixels)[x+z*512]=Game_Map[x, y, z].icol;
		}
	}*/
	Renderer.LoadMap(Game_Map);
}

//NOTE: Actually these don't belong here, but a renderer can bring its own map memory format
bool Voxel_IsSolid(Tx, Ty, Tz)(Tx x, Ty y, Tz z){
	return Game_Map[x, y, z].S!=0;
}
uint Voxel_GetHighestY(Tx, Ty, Tz)(Tx xpos, Ty ypos, Tz zpos){
	foreach(y; (cast(size_t)ypos)..Game_Map.ysize){
		if(Voxel_IsSolid(xpos, y, zpos))return cast(uint)y;
	}
	return Game_Map.ysize-1;
}

void Voxel_SetColor(Tx, Ty, Tz)(Tx xpos, Ty ypos, Tz zpos, uint col){
	bool update_minimap=false;
	if(Render_MiniMap){
		if(!Voxel_IsSolid(xpos, ypos, zpos)){
			if(Voxel_GetHighestY(xpos, 0, zpos)>ypos){
				update_minimap=true;
			}
		}
	}
	Game_Map[xpos, ypos, zpos]=col;
	if(update_minimap){
		*Pixel_Pointer(minimap_srfc, xpos, zpos)=Voxel_GetColor(xpos, ypos, zpos)&0x00ffffff;
		MiniMap_SurfaceChanged=true;
	}
}

void Voxel_Remove(uint xpos, uint ypos, uint zpos){
	Game_Map[xpos, ypos, zpos].S=0;
}

void Voxel_SetShade(uint x, uint y, uint z, ubyte shade){
}

ubyte Voxel_GetShade(uint x, uint y, uint z){
	return 0;
}

uint Voxel_GetColor(uint x, uint y, uint z){
	return Game_Map[x, y, z].icol&0x00ffffff;
}

void Renderer_Blit2D(RendererTexture_t tex, uint[2]* size, SDL_Rect *dstr, ubyte alpha=255, ubyte[3] *ColorMod=null, SDL_Rect *srcr=null){
}

void Renderer_SetFog(uint fogcolor, uint fogrange){}

void Renderer_Finish2D(){
	SDL_GL_SwapWindow(scrn_window);
}

void Renderer_UnInit(){
}

void Renderer_ShowInfo(){}

void Renderer_FinishRendering(){
}

void Renderer_StartRendering(bool Render_3D){
}

void Renderer_SetBlur(real amount){}


alias Renderer_DrawWireframe=Renderer_DrawSprite;

void Renderer_DrawVoxels(){
	if(Renderer)
		Renderer.Draw();
}


void Renderer_AddFlash(Vector3_t pos, float radius, float brightness){}
void Renderer_Start2D(){}

void Renderer_DrawSmokeCircle(immutable in float xpos, immutable in float ypos,
immutable in float zpos, immutable in int radius, immutable in uint color, immutable in ubyte alpha, immutable in float dist){}

void Renderer_DrawLine2D(int x1, int y1, int x2, int y2, ubyte[4] *color){}
void Renderer_SetBrightness(float brightness){}
void Renderer_SetBlockFaceShading(Vector3_t shading){}
auto Renderer_DrawRoundZoomedIn(Vector3_t* scope_pos, Vector3_t* scope_rot, MenuElement_t *scope_picture, float xzoom, float yzoom){
		struct return_type{
		SDL_Rect dstrect;
		SDL_Rect srcrect;
		SDL_Texture *scope_texture;
		uint scope_texture_width, scope_texture_height;
	}
	return_type ret;
	return ret;
}
void Renderer_UpdateFlashes(alias UpdateGfx=true)(float update_speed){}
void _Register_Lighting_BBox(int xpos, int ypos, int zpos){}
