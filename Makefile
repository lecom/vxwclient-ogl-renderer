SRCFILES = *.d *.di
DEPENDENCIES=./derelict/enet/*.d ./derelict/openal/*.d ./derelict/vorbis/*.d ./derelict/ogg/*.d ./derelict/util/*.d ./SDLang2/sdl2.d ./SDLang2/sdl2i.di
#GFM_INT_FILES=vxwclient-ogl-renderer/gfm/integers/gfm/integers/*.d
#GFM_MATH_FILES=vxwclient-ogl-renderer/gfm/math/gfm/math/*.d
GLEN_FILES=vxwclient-ogl-renderer/gl3n/gl3n/*.d
GLAD_FILES=vxwclient-ogl-renderer/glad/gl/*.d
RENDERER_LIB_SRC=$(GLAD_FILES) $(GLEN_FILES) vxwclient-ogl-renderer/renderer.d vxwclient-ogl-renderer/ogl_renderer.d vxwclient-ogl-renderer/renderer_templates.d
RENDERER_LIB_LL=
DFLAGS=
TARGET_ARCH=$(shell getconf LONG_BIT)
ARCH_EXTENSIONS=mmx sse sse2 sse3 #sse4.1 ssse3 cx16 cmov
LDC_ARCH_EXT=$(shell arch_ext=; for i in $(ARCH_EXTENSIONS); do arch_ext="$$arch_ext -mattr=$$i"; done; echo $$arch_ext)
GDC_ARCH_EXT=$(shell arch_ext=; for i in $(ARCH_EXTENSIONS); do arch_ext="$$arch_ext -m$$i"; done; echo $$arch_ext)
D_LDFLAGS=$(shell ld_flags=; for i in $(LD_FLAGS); do ld_flags="$$ld_flags -L$$i"; done; echo $$ld_flags)
C_LDFLAGS=$(LDFLAGS)
CLANG_VER=-4.0

dmd:
	dmd -m$(TARGET_ARCH) $(DFLAGS) $(RENDERER_LIB_SRC) -unittest $(SRCFILES) $(DEPENDENCIES) \
	-L-L/usr/local/lib -L-lGL -L-lGLU -L-L. -L-lSDL2 -L-lSDL2_image -L-lslang -ofmain

ldc: $(RENDERER_LIB_SRC)
	ldc2 $(LDC_ARCH_EXT) -m$(TARGET_ARCH) $(DFLAGS) \
	-singleobj $(SRCFILES) $(DEPENDENCIES) $(RENDERER_LIB_SRC) -L-L/usr/local/lib -L-lGL -L-lGLU -L-lslang -L-lSDL2 -L-lSDL2_image \
 	-ofmain

gdc: $(RENDERER_LIB_OBJ) $(RENDERER_LIB_SRC)
	gdc -pg -march=native $(GDC_ARCH_EXT) -m$(TARGET_ARCH) $(DFLAGS) $(SRCFILES) $(DEPENDENCIES) $(RENDERER_LIB_SRC) \
	-Wl,-lSDL2 -Wl,-lSDL2_image -Wl,-ldl -Wl,-L/usr/local/lib -Wl,-lslang -Wl,-lGL -Wl,-lGLU -Wl,-lz -omain

derelict_obj.o:
	ldc2 -m$(TARGET_ARCH) -singleobj -release $(DEPENDENCIES) -c -ofderelict_obj.o

.PHONY: clean
clean:
	rm *.o *.ll

.PHONY: distclean
distclean:
	rm *.o main *.ll
