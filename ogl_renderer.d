import std.string; // toStringz, fromStringz
import std.math;   // PI
import std.conv;
import glad.gl.all;
import glad.gl.loader;
import gl3n.math;
import gl3n.linalg;
import std.exception;
import sdl2;
import misc;
import renderer;

struct Shader {
	GLuint ID;
	this(string vertexPath, string fragmentPath, string function(string) getFile) {
		string vCode = getFile(vertexPath);
		string fCode = getFile(fragmentPath);
 
		const char * vShaderCode = toStringz(vCode);
		const char * fShaderCode = toStringz(fCode);
 
		GLuint vertex, fragment;
 
		vertex = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertex, 1, &vShaderCode, null);
		glCompileShader(vertex);
		Shader_OK(vertex, "VERTEX", vCode, __LINE__);
 
		fragment = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragment, 1, &fShaderCode, null);
		glCompileShader(fragment);
		Shader_OK(fragment, "FRAGMENT", fCode, __LINE__);
 
		this.ID = glCreateProgram();
		glAttachShader(ID, vertex);
		glAttachShader(ID, fragment);
		glLinkProgram(ID);
		Shader_OK(ID, "PROGRAM", "", __LINE__);
 
		glDeleteShader(vertex);
		glDeleteShader(fragment);
	}
 
	void use() {
		glUseProgram(this.ID);
	}
 
	void setInt(const string name, int value) const {
		glUniform1i(glGetUniformLocation(ID, toStringz(name)), value);
	}
	void SetFloat(const string name, float value) const {
		glUniform1f(glGetUniformLocation(ID, toStringz(name)), value);
	}
	void SetVec2(const string name, const ref vec2 value) const {
		glUniform2fv(glGetUniformLocation(ID, toStringz(name)), 1, value.value_ptr);
	}
	void SetVec3(const string name, const ref vec3 value) const {
		glUniform3fv(glGetUniformLocation(ID, toStringz(name)), 1, value.value_ptr); 
	}
	void SetVec4(const string name, const ref vec4 value) const {
		glUniform4fv(glGetUniformLocation(ID, toStringz(name)), 1, value.value_ptr); 
	}
	void SetMat2(const string name, const ref mat2 mat) const{
		mat2 _mat=mat; _mat.transpose();
		glUniformMatrix2fv(glGetUniformLocation(ID, toStringz(name)), 1, GL_TRUE, _mat.value_ptr);
	}
	void SetMat3(const string name, const ref mat3 mat) const{
		mat3 _mat=mat; _mat.transpose();
		glUniformMatrix3fv(glGetUniformLocation(ID, toStringz(name)), 1, GL_TRUE, _mat.value_ptr);
	}
	void SetMat4(const string name, const ref mat4 mat) const{
		mat4 _mat=mat; _mat.transpose();
		glUniformMatrix4fv(glGetUniformLocation(ID, toStringz(name)), 1, GL_TRUE, _mat.value_ptr);
	}
 
	private void Shader_OK(uint shader, string type, string shaderprog, int caller_line) {
		GLint success;
		GLchar[1024] infoLog; infoLog[]=0;
		if (type == "PROGRAM") {
			glGetProgramiv(shader, GL_LINK_STATUS, &success);
		} else {
			glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
		}
		if (success!=GL_TRUE) {
			GLsizei loglen;
			if(type=="PROGRAM")
				glGetProgramInfoLog(shader, 1024, &loglen, infoLog.ptr);
			else
				glGetShaderInfoLog(shader, 1024, &loglen, infoLog.ptr);
			throw new Exception("Error on line "~caller_line.to!string()~";"~cast(string)fromStringz(infoLog.ptr)~"\n"~"Shader\n"~shaderprog);
		}
	}
}
 
class Helpers {
	public static mat4 lookAt(vec3d eye, vec3d center, vec3d up) {
		vec3d f = center - eye;
		f.normalize();
		vec3d s = cross(f, up);
		s.normalize();
		vec3d u = cross(s, f);
 
		mat4 result = mat4(0.0f);
		result[0][0] = s.x;
		result[1][0] = s.y;
		result[2][0] = s.z;
		result[0][1] = u.x;
		result[1][1] = u.y;
		result[2][1] = u.z;
		result[0][2] =-f.x;
		result[1][2] =-f.y;
		result[2][2] =-f.z;
		result[3][0] =-dot(s, eye);
		result[3][1] =-dot(u, eye);
		result[3][2] = dot(f, eye);
 
		return result;
	}
 
	public static mat4 perspective(double fovy, double aspect, double zNear, double zFar) {
		enforce(aspect != 0.0f);
		enforce(zNear != zFar);
 
		double thv = tan(fovy / 2.0f);
 
		mat4 result = mat4(0.0f);
		result[0][0] = 1.0f / (aspect * thv);
		result[1][1] = 1.0f / (thv);
		result[2][2] = - (zFar + zNear) / (zFar - zNear);
		result[2][3] = -1.0f;
		result[3][2] = - (2.0f * zFar * zNear) / (zFar - zNear);
		
		return result;
	}
 
	public static double radians(double degrees) {
		return (PI * degrees) / 180.0f;
	}
	
	public static mat4 identity(){
		mat4 ret=mat4(0.0f);
		ret[0][0]=1.0f;
		ret[1][1]=1.0f;
		ret[2][2]=1.0f;
		ret[3][3]=1.0f;
		return ret;
	}
}
 
class Camera {
	vec3d position;
	double yaw;
	double pitch;
	double fovY;
	double asp;
 
	void SetCamera(double fovY, double asp, vec3d pos, double yaw, double pitch) {
		this.fovY = fovY;
		this.asp = asp;
		this.position = pos;
		this.yaw = yaw;
		this.pitch = pitch;
	}
 
	mat4 GetViewMatrix(){
		vec3d front;
		front.x = cos(Helpers.radians(this.yaw)) * cos(Helpers.radians(this.pitch));
		front.y = sin(Helpers.radians(this.pitch));
		front.z = sin(Helpers.radians(this.yaw)) * cos(Helpers.radians(this.pitch));
		front.normalize();
 
		return Helpers.lookAt(this.position, this.position + front, vec3d(0.0f, 1.0f, 0.0f));
	}
	
	mat4 GetPerspective(){
		return Helpers.perspective(fovY, asp, .1f, 100.0f);
	}
}
 
class Renderer {
	this(string function(string) getFile) {
		void *__load(const(char) *name){return SDL_GL_GetProcAddress(name);}
		enforce(gladLoadGL(&__load), "Unable to load OpenGL");
		writeflnlog("OpenGL loaded");
		writeflnlog("Vendor:   %s", fromStringz(cast(char*)glGetString(GL_VENDOR)));
		writeflnlog("Renderer: %s", fromStringz(cast(char*)glGetString(GL_RENDERER)));
		writeflnlog("Version:  %s", fromStringz(cast(char*)glGetString(GL_VERSION)));
		glEnable(GL_DEPTH_TEST);
 
		lightingShader=Shader("materials.vs.glsl", "materials.fs.glsl", getFile);
		lampShader=Shader("lamp.vs.glsl", "lamp.fs.glsl", getFile);
		cam=new Camera();
	}
 
	void ResizeWindow(uint windowX, uint windowY) {
		glViewport(0, 0, windowX, windowY);
	}
 
	void SetCamera(double fovY, double asp, double x, double y, double z, double yaw, double pitch) {
		cam.SetCamera(fovY, asp, vec3d(x, y, z), yaw, pitch);
	}
	
	struct DrawBlock_t{
		float xpos, ypos, zpos;
		float colr, colg, colb;
	}
	DrawBlock_t[] draw_blocks;
	
	uint Terrain_VoxelVAO;
	uint Terrain_VoxelVBO;
	uint Terrain_VoxelVerticesVBO;
 
	void LoadMap(Map_t map){
		foreach(x; 0..map.xsize){
			foreach(y; 0..map.ysize){
				foreach(z; 0..map.zsize){
					if(map[x, y, z].S && map.isSurfaceBlock(x, y, z)){
						if(y==63)
							map[x, y, z].R=255;
						draw_blocks~=DrawBlock_t(x, y, z, map[x, y, z].R/255.0f, map[x, y, z].G/255.0f, map[x, y, z].B/255.0f);
					}
				}
			}
		}
		
		//blockVertices=generate_cube_triangles();
		
		//Static Buffer that never changes
		glGenBuffers(1, &Terrain_VoxelVerticesVBO); CheckError(__LINE__);
		glBindBuffer(GL_ARRAY_BUFFER, Terrain_VoxelVerticesVBO); CheckError(__LINE__);
		glBufferData(GL_ARRAY_BUFFER, blockVertices.length*blockVertices[0].sizeof, blockVertices.ptr, GL_STATIC_DRAW); CheckError(__LINE__);
		glBindBuffer(GL_ARRAY_BUFFER, 0); CheckError(__LINE__);
		
		glGenVertexArrays(1, &Terrain_VoxelVAO); CheckError(__LINE__);
		glGenBuffers(1, &Terrain_VoxelVBO); CheckError(__LINE__);
	}
	
	float[] blockVertices=[
	0.0f,0.0f,0.0f, 0.0f,0.0f, 1.0f, 0.0f, 1.0f, 1.0f,	1.0f, 1.0f,0.0f, 0.0f,0.0f,0.0f, 0.0f, 1.0f,0.0f,
	 1.0f,0.0f, 1.0f, 0.0f,0.0f,0.0f,  1.0f,0.0f,0.0f,   	1.0f, 1.0f,0.0f,  1.0f,0.0f,0.0f, 0.0f,0.0f,0.0f,
	0.0f,0.0f,0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,0.0f,	1.0f,0.0f, 1.0f, 0.0f,0.0f, 1.0f, 0.0f,0.0f,0.0f,
	0.0f, 1.0f, 1.0f, 0.0f,0.0f, 1.0f,  1.0f,0.0f, 1.0f,	1.0f, 1.0f, 1.0f,  1.0f,0.0f,0.0f,  1.0f, 1.0f,0.0f,
	 1.0f,0.0f,0.0f,  1.0f, 1.0f, 1.0f,  1.0f,0.0f, 1.0f,	1.0f, 1.0f, 1.0f,  1.0f, 1.0f,0.0f, 0.0f, 1.0f,0.0f,
	 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,0.0f, 0.0f, 1.0f, 1.0f,	1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f,  1.0f,0.0f, 1.0f
	];
	
	float[] create_face_triangles(float[3] ul, float[3] ur, float [3] ll, float[3] lr){
		return (ll~ul~ur)~(ll~ur~lr);
	}
	
	float[] generate_cube_triangles(){
		float[3] upper_ul=[0.0, 0.0, 0.0], upper_ur=[1.0, 0.0, 0.0], upper_ll=[0.0, 0.0, 1.0], upper_lr=[1.0, 0.0, 1.0];
		float[3] lower_ul=[0.0, 1.0, 0.0], lower_ur=[1.0, 1.0, 0.0], lower_ll=[0.0, 1.0, 1.0], lower_lr=[1.0, 1.0, 1.0];
		return create_face_triangles(upper_ul, upper_ur, upper_ll, upper_lr)~ //Upper face
		create_face_triangles(lower_ul, lower_ur, lower_ll, lower_lr)~ //Lower face
		create_face_triangles(upper_ul, upper_ll, lower_ll, lower_ul)~//"Left" face
		create_face_triangles(upper_lr, upper_ur, lower_ur, lower_lr)~//"Right" face
		create_face_triangles(upper_ur, upper_ul, lower_ur, lower_ul)~//Back face
		create_face_triangles(upper_ll, upper_lr, lower_lr, lower_ll)//Front face
		;
	}
	
	void Draw(){
		glBindVertexArray(Terrain_VoxelVAO); CheckError(__LINE__);
		glBindBuffer(GL_ARRAY_BUFFER, Terrain_VoxelVBO); CheckError(__LINE__);
		glBufferData(GL_ARRAY_BUFFER, draw_blocks.length*draw_blocks[0].sizeof, draw_blocks.ptr, GL_STATIC_DRAW); CheckError(__LINE__);
		glEnableVertexAttribArray(0); CheckError(__LINE__);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*DrawBlock_t.xpos.sizeof, cast(void*)0); CheckError(__LINE__);
		glVertexAttribDivisor(0, 1); CheckError(__LINE__);
		glEnableVertexAttribArray(1); CheckError(__LINE__);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*DrawBlock_t.xpos.sizeof, cast(void*)(3*DrawBlock_t.xpos.sizeof)); CheckError(__LINE__);
		glVertexAttribDivisor(1, 1); CheckError(__LINE__);
		
		glEnableVertexAttribArray(2); CheckError(__LINE__);
		glBindBuffer(GL_ARRAY_BUFFER, Terrain_VoxelVerticesVBO); CheckError(__LINE__);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 3*blockVertices[0].sizeof, cast(void*)0); CheckError(__LINE__);
		glBindBuffer(GL_ARRAY_BUFFER, 0); CheckError(__LINE__);
		glBindVertexArray(0); CheckError(__LINE__);
		//glVertexAttribDivisor(2, 0); CheckError(__LINE__);
		
		// Enable depth test
		glEnable(GL_DEPTH_TEST);
		// Accept fragment if it closer to the camera than the former one
		glDepthFunc(GL_LESS);
		
		glClearColor(.2f, .2f, .2f, .2f); CheckError(__LINE__);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); CheckError(__LINE__);
		lightingShader.use(); CheckError(__LINE__);
		auto mat_perspective=cam.GetPerspective(); lightingShader.SetMat4("projection", mat_perspective); CheckError(__LINE__);
		auto mat_view=cam.GetViewMatrix(); lightingShader.SetMat4("view", mat_view); CheckError(__LINE__);
		auto mat_model=Helpers.identity(); lightingShader.SetMat4("model", mat_model); CheckError(__LINE__);
		glBindVertexArray(Terrain_VoxelVAO); CheckError(__LINE__);
		glDrawArraysInstanced(GL_TRIANGLES, 0, 12, cast(int)draw_blocks.length); CheckError(__LINE__);
		glBindVertexArray(0); CheckError(__LINE__);
	}
	
	void CheckError(int line){
		GLenum err=glGetError();
		if(err!=GL_NO_ERROR){
			writeflnerr("GL ERROR on line %s: %s", line, fromStringz(gluErrorString(err)));
		}
	}
	
	/*void UnloadMap();
 
	void AddBlock(uint x, uint y, uint z, ubyte r, ubyte g, ubyte b);
	void RemoveBlock(uint x, uint y, uint z);
 
	void Draw();*/
 
	//~this();
 
	private Shader lightingShader;
	private Shader lampShader;
	private Camera cam;
}
